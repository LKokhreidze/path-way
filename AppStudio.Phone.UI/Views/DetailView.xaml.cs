using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Net.NetworkInformation;

using Microsoft.Phone.Controls;

using MyToolkit.Paging;

using AppStudio.Data;
using AppStudio.Services;
using AppStudio.Views;
using Windows.Storage;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Windows.Media;

namespace AppStudio
{
    public partial class DetailView
    {
        private StorageFolder _local = ApplicationData.Current.LocalFolder;
        private StorageFolder _localFolder;
        public static bool IsImageClickAvailable { get; set; }
        public DetailView()
        {
            InitializeComponent();
        }

        public ToursViewModel ToursViewModel { get; private set; }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ToursViewModel = NavigationServices.CurrentViewModel as ToursViewModel;
            ToursViewModel.ViewType = ViewTypes.Detail;            
            DataContext = ToursViewModel;
            string parameter;
            if (NavigationContext.QueryString.TryGetValue("error", out parameter)) {
                MessageBox.Show("You need to download the tour!");
            }
            base.OnNavigatedTo(e);
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            NavigationServices.NavigateTo(new Uri("/Views/MainPage.xaml", UriKind.Relative));
        }

        private async void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            _localFolder = await _local.CreateFolderAsync("KMLFolder", CreationCollisionOption.OpenIfExists);
            if (ToursDataSource.CheckNetworkConnection())
	            {
                    await DownloadKML(ToursViewModel.SelectedItem.KMLUrl, ToursViewModel.SelectedItem.Title);
                    if (File.Exists(Path.Combine(_localFolder.Path, ToursViewModel.SelectedItem.Title)))
                    {
                        MessageBox.Show(String.Format("Download Completed\nPress the picture to see the tour"));
                        btnDownload.Visibility = System.Windows.Visibility.Collapsed;
                        IsImageClickAvailable = true;
                    } else {
                        MessageBox.Show("Error Downloading file. Please Try Again Later");
                    }
                    
            } else {
                MessageBox.Show("Problem with connection");
            }      
        }
       
        public static Task<Stream> DownloadFile(Uri url) {
            var htp = new HttpClient();
            return htp.GetStreamAsync(url);
        }

        public async Task WriteDownloadedFiles(Uri uriToDownload, string fileName) {
            try {
                using (Stream myStr = await DownloadFile(uriToDownload)) {
                    StorageFile storageFile = await _localFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                    using (Stream outputStream = await storageFile.OpenStreamForWriteAsync()) {
                        await myStr.CopyToAsync(outputStream);
                    }
                }
            } catch (Exception exc) {
                MessageBox.Show(exc.Message);
            }
        }

        private async Task DownloadKML(string url, string name) {
            _localFolder = await _local.CreateFolderAsync("KMLFolder", CreationCollisionOption.OpenIfExists);
            await WriteDownloadedFiles(new Uri(url, UriKind.Absolute), name);
        }


        private async void Container_Loaded(object sender, RoutedEventArgs e)
        {
            _localFolder = await _local.CreateFolderAsync("KMLFolder", CreationCollisionOption.OpenIfExists);
            if (File.Exists(Path.Combine(_localFolder.Path,ToursViewModel.SelectedItem.Title)))
            {
                btnDownload.Visibility = System.Windows.Visibility.Collapsed;
                IsImageClickAvailable = true;
            }
            else
            {
                btnDownload.Visibility = System.Windows.Visibility.Visible;
                IsImageClickAvailable = false;
            }
        }

        private async void Container_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            _localFolder = await _local.CreateFolderAsync("KMLFolder", CreationCollisionOption.OpenIfExists);
            if (File.Exists(Path.Combine(_localFolder.Path, ToursViewModel.SelectedItem.Title)))
            {
                btnDownload.Visibility = System.Windows.Visibility.Collapsed;
                IsImageClickAvailable = true;
            }
            else
            {
                btnDownload.Visibility = System.Windows.Visibility.Visible;
                IsImageClickAvailable = false;
            }
        }
    }
}
