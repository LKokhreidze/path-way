﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AppStudio.Data;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;

namespace AppStudio.Views
{
    public partial class PhotosArround : PhoneApplicationPage
    {
        public PhotosArround()
        {
            InitializeComponent();
        }

        private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            Overlay.Visibility = System.Windows.Visibility.Visible;
            PhotoProgressBar.IsIndeterminate = true;
            if (ToursDataSource.CheckNetworkConnection()) {
                var images = await FlickrDataProvider.DisplayPhotos();
                DataContext = images;

                SearchResult.Visibility = images.Count == 0 ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;

                Overlay.Visibility = System.Windows.Visibility.Collapsed;
                PhotoProgressBar.IsIndeterminate = false;
            } else {
                txt_Result.Text = "Master, I Need Internet Connection";
            }
            
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            Image img = sender as Image;

            if (img == null)
            {
                return;
            }

            Storyboard story = new Storyboard();

            DoubleAnimation doubleAni = new DoubleAnimation();
            doubleAni.To = 1;
            doubleAni.Duration = new Duration(TimeSpan.FromSeconds(1));

            Storyboard.SetTarget(doubleAni, img);
            Storyboard.SetTargetProperty(doubleAni, new PropertyPath(OpacityProperty));

            story.Children.Add(doubleAni);

            story.Begin();
        }
    }
}