using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using AppStudio.Services;
using System.ComponentModel;
using AppStudio.Data;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Microsoft.Phone.Tasks;

namespace AppStudio {
    public partial class MainPage {
        private Panorama _panorama;
        private static PhoneTextBox _txtSearch;
        private static PhoneTextBox _mail;
        private static PhoneTextBox _subject;
        private static PhoneTextBox _body;

        public MainViewModels MainViewModels { get; private set; }

        public MainPage() {
            InitializeComponent();
            if (MapServices.IsLocationServiceEnabled)
            {
                MainViewModels = new MainViewModels();
                BitmapImage img = Application.Current.Resources["AppBackgroundImage"] as BitmapImage;
                string url = RandomImage();
                img.UriSource = new Uri(url, UriKind.Relative); 
            }
            else
            {
                MessageBox.Show("Your location services are turned off. To turn them on, go to location in your phone settings");
                Application.Current.Terminate();
            }
            
        }

        private void Container_Loaded(object sender, RoutedEventArgs e) {
            _panorama = sender as Panorama;
            foreach (PhoneTextBox item in FindVisualChildren<PhoneTextBox>(this)) {
                if (item.Name == "txt_mail") {
                    _mail = item;
                }
                if (item.Name == "txt_subject") {
                    _subject = item;
                }
                if (item.Name == "txt_body") {
                    _body = item;
                }
                if (item.Name == "txt_search") {
                    _txtSearch = item;
                }
            }
            _mail.LostFocus += PhoneTextBox_LostFocus;
            _mail.GotFocus += _txtSearch_GotFocus;

            _subject.LostFocus += PhoneTextBox_LostFocus;
            _subject.GotFocus += _txtSearch_GotFocus;

            _body.LostFocus += PhoneTextBox_LostFocus;
            _body.GotFocus += _txtSearch_GotFocus;

            _txtSearch.LostFocus += PhoneTextBox_LostFocus;
            _txtSearch.GotFocus += _txtSearch_GotFocus;
            _txtSearch.TextChanged += SearchClick;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (_panorama != null) {
                var item = _panorama.SelectedItem as PanoramaItem;
                if (item != null) {
                    MainViewModels.SelectedItem = item.Content as ViewModelBase;
                }
            }
            if (_txtSearch.Text != "") {
                _txtSearch.Visibility = System.Windows.Visibility.Visible;
            } else {
                _txtSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e) {
            NavigationService.RemoveBackEntry();
            MainViewModels.SetViewType(ViewTypes.List);
            DataContext = MainViewModels;
            MainViewModels.UpdateAppBar();
            await MainViewModels.LoadData(NetworkInterface.GetIsNetworkAvailable());
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            base.OnNavigatedFrom(e);
        }

        private string RandomImage() {
            var image = new List<string> { 
                "/Assets/DataImages/autumn_mountain_trail1.jpg", 
                "/Assets/DataImages/mountains.jpg", 
                "/Assets/DataImages/tbilisi.jpg", 
                "/Assets/DataImages/racha.jpg" 
            };
            Random rnd = new Random();
            int index = rnd.Next(image.Count);
            var name = image[index];
            image.RemoveAt(index);
            return name;
        }

        public IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject {
            if (depObj != null) {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T) {
                        yield return (T)child;
                    }
                    foreach (T childOfChild in FindVisualChildren<T>(child)) {
                        yield return childOfChild;
                    }
                }
            }
        }

        protected override void OnBackKeyPress(CancelEventArgs e) {
            if (_panorama.SelectedIndex != 0) {
                _panorama.DefaultItem = _panorama.Items[0];
                e.Cancel = true;
            } else {
                Application.Current.Terminate();
            }
        }

        void PhoneTextBox_LostFocus(object sender, RoutedEventArgs e) {
            switch ((sender as PhoneTextBox).Name)
            {
                case "txt_search":
                    _txtSearch.Foreground = App.Current.Resources["PhoneForegroundBrush"] as Brush;
                    break;
                case "txt_mail":
                    _mail.Foreground = App.Current.Resources["PhoneForegroundBrush"] as Brush;
                    break;
                case "txt_subject":
                    _subject.Foreground = App.Current.Resources["PhoneForegroundBrush"] as Brush;
                    break;
                case "txt_body":
                    _body.Foreground = App.Current.Resources["PhoneForegroundBrush"] as Brush;
                    break;
            }
        }

        void _txtSearch_GotFocus(object sender, RoutedEventArgs e) {
            switch ((sender as PhoneTextBox).Name)
            {
                case "txt_search":
                    _txtSearch.Foreground = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    _txtSearch.BorderBrush = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    break;
                case "txt_mail":
                    _mail.Foreground = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    _mail.BorderBrush = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    break;
                case "txt_subject":
                     _subject.Foreground = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    _subject.BorderBrush = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                       break;
                case "txt_body":
                   _body.Foreground = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
            _body.BorderBrush = App.Current.Resources["PhoneBackgroundBrush"] as Brush;
                    break;
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e) {
            if (_txtSearch.Visibility == System.Windows.Visibility.Collapsed) {
                _txtSearch.Visibility = System.Windows.Visibility.Visible;
            } else if (_txtSearch.Visibility == System.Windows.Visibility.Visible) {
                _txtSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private async void SearchClick(object sender, EventArgs e) {
            ToursDataSource tr = new ToursDataSource();
            await tr.Search(_txtSearch.Text);
        }

        private void btn_send_Click(object sender, EventArgs e) {
            EmailComposeTask mail = new EmailComposeTask();
            mail.Body = _body.Text;
            mail.To = "travelgis@gmail.com";
            mail.Subject = _subject.Text;
            mail.Show();
        }
    }
}
