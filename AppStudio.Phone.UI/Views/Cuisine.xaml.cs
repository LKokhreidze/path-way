﻿using System;
using System.Collections.Generic;
using Microsoft.Phone.Controls;
using AppStudio.Services;

namespace AppStudio.Views {
    public partial class Cuisine : PhoneApplicationPage {
        public Cuisine() {
            InitializeComponent();
            AddFoods();
        }

        private void AddFoods() {
            List<Foods> FoodList = new List<Foods>();
            FoodList.Add(new Foods()
            {
                FoodTitle = "Mtsvadi",
                FoodDescription = String.Format("is one of the most popular and traditional Georgian dish. It's dish for special occasions. It's made from meat mostly pork or lamb. The meat is rubbed down with solt, pepper, onions after that it is left in wine or vinegar for couple of hours. Afterwards it is put special spit which is placed over hot coals to roast for about 15 minutes. Then the meat slid off the skewer and onto a waiting plate before being brought to the table, hot and delicious."),
                FoodImage = @"/Assets/Foods/mtsvadi.png"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Khachapuri",
                FoodDescription = String.Format("is a traditional Georgian dish of cheese-filled bread. \nThe bread is leavened and allowed to rise, and is shaped in various ways. The filling contains cheese (fresh or aged, most commonly suluguni), eggs and other ingredients."),
                FoodImage = @"/Assets/Foods/khachapuri.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Lobiani",
                FoodDescription = String.Format(" is a traditional Georgian dish of bean-filled bread.\n The word 'Lobiani' comes from the Georgian word for beans which is 'Lobio'. This 'Lobio' or the kidney beans is the most important ingredient for making Lobiani."),
                FoodImage = @"/Assets/Foods/lobiani.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Mchadi",
                FoodDescription = "Mchadi is a traditional Georgian cornbread traditionally eaten with Lobio and cheese.",
                FoodImage = @"/Assets/Foods/mchadi.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Chakhokhbili",
                FoodDescription = String.Format("is a traditional Georgian dish of a tarragon-infused spicy beef soup and stewed chicken with fresh herbs.\n Its name comes from the Georgian word (khokhobi) which means pheasant."),
                FoodImage = @"/Assets/Foods/chakhokhbili.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Chakapuli",
                FoodDescription = String.Format("is a Georgian stew. It is considered to be one of the most popular dishes in Georgia. \nIt is made from onions, lamb chops, dry white wine, tarragon leaves, tkemali (plum sauce), mixed fresh herbs (parsley, mint, dill, cilantro), garlic and salt."),
                FoodImage = @"/Assets/Foods/chakapuli.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Chikhirtma",
                FoodDescription = String.Format("is a traditional Georgian soup. Chikhirtma is described as a soup almost completely without a vegetable base.\n The soup made from a rich chicken (or less commonly, lamb) broth, which is thickened with beaten eggs (or only yolks). The technique of inclusion of uncooked eggs to hot broth is rather difficult. The technique appeared in Ancient Persia and is intended for creation of intimate blend of broth and eggs."),
                FoodImage = @"/Assets/Foods/chixirtma.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Chanakhi",
                FoodDescription = String.Format("is a traditional Georgian dish of lamb stew with tomatoes, aubergines, potatoes, greens and garlic."),
                FoodImage = @"/Assets/Foods/chanaxi.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Khinkali",
                FoodDescription = String.Format("is a Georgian dumpling which originated in the Georgian regions of Pshavi, Mtiuleti and Khevsureti. Varieties of Khinkali spread from there across different parts of the Caucasus. Khinkali is filled with various fillings, mostly with spiced meat (usually beef and pork in Georgia, beef in Azerbaijan and other Muslim-majority areas, and sometimes lamb), herbs (usually coriander), and onions. Mushrooms, potatoes, or cheese may be used in place of meat."),
                FoodImage = @"/Assets/Foods/xinkali.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Ajika",
                FoodDescription = String.Format("Ajika or Adjika (Georgian: აჯიკა, Abkhaz: Aџьыка) is a Georgian hot, spicy but subtly flavoured dip often used to flavour food.\n Ajika is usually red, though green ajika can be made with unripe peppers. The name itself comes from the Abkhaz word salt (the more descriptive аџьыкаҟaҧшь (literally, red salt) and аџьыкаҵәаҵәа are also used to refer specifically to ajika).\n The Abkhazian variant of ajika is based on a boiled preparation of hot red peppers, garlic, herbs and spices such as coriander, dill, blue fenugreek (only found in mountain regions such as the Alps or the Caucasus), salt and walnut. A dry form of ajika exists that is sometimes called svanuri marili in Georgian (სვანური მარილი Svanetian salt); this looks like small red clumps mixed with a looser version of the spice mixture"),
                FoodImage = @"/Assets/Foods/Ajika.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Tkemali",
                FoodDescription = String.Format("is the Georgian sour plum sauce made of such cherry plums. Tkemali is made from both red and green varieties of plum. The flavor of the sauce varies, but generally tends to be pungently tart. To lower the tartness level, occasionally sweeter types of plums are added during preparation. Traditionally the following ingredients are used besides plum: garlic, pennyroyal, coriander, dill, chili pepper and salt."),
                FoodImage = @"/Assets/Foods/tkemali.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Churchkhela",
                FoodDescription = String.Format("are traditional sausage-shaped candies originating from Georgia. It is also popular in Russia, Cyprus, Greece and Turkey. In Turkish it is called pestil cevizli sucuk, literally walnut sujuk because of its sausage shape.\n The main ingredients are grape must, nuts and flour. Almonds, walnuts, hazel nuts and sometimes raisins are threaded onto a string, dipped in thickened grape juice or fruit juices and dried in the shape of a sausage"),
                FoodImage = @"/Assets/Foods/Curcxela.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Gozinaki",
                FoodDescription = String.Format("is a traditional Georgian confection made of caramelized nuts, usually walnuts, fried in honey, and served exclusively on New Year's Eve and Christmas. In the western Georgian provinces of Imereti and Racha, it was sometimes called Churchkhela, a name more commonly applied to walnuts sewn onto a string, dipped in thickened white grape juice and dried. In several of Georgia's rural areas, both walnuts and honey used to have sacral associations. According to a long established tradition, Gozinaqi is a mandatory component of New Year's Eve/Christmas celebration."),
                FoodImage = @"/Assets/Foods/Gozinaki.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Kharcho",
                FoodDescription = String.Format("is a traditional Georgian soup containing beef, rice, cherry plum purée and chopped English walnut (Juglans regia). The soup is usually served with finely chopped fresh coriander. The characteristic ingredients of the soup are meat, cherry plum purée made from (tklapi/tkemali), rice, chopped English walnut and the spice mix that varies between different regions of Georgia."),
                FoodImage = @"/Assets/Foods/kharcho.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Pkhali",
                FoodDescription = String.Format("is a traditional Georgian dish of chopped and minced vegetables including cabbage, eggplant, spinach, beans, beets and combined with ground walnuts, vinegar, onions, garlic, and herbs"),
                FoodImage = @"/Assets/Foods/Pkhali.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Kuchmachi",
                FoodDescription = String.Format("is a traditional Georgian dish of chicken livers, hearts and gizzards with walnuts and pomegranate seeds for topping."),
                FoodImage = @"/Assets/Foods/Kuchmachi.jpg"
            });
            FoodList.Add(new Foods() {
                FoodTitle = "Kupati",
                FoodDescription = String.Format("is a type of Georgian sausage that is made from pork. It's popular in the Caucasus region."),
                FoodImage = @"/Assets/Foods/kupati.jpg"
            });
            DataContext = FoodList;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationServices.NavigateToPage("MainPage");
        }
    }
    class Foods {
        private string _ImageUrl = "/Assets/NoImage.jpg";
        public string FoodTitle { get; set; }
        public string FoodDescription { get; set; }
        public string FoodImage {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }
    }
}