﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AppStudio.Data;
using AppStudio.Services;
using System.ComponentModel;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Maps.Controls;
using Kml;
using System.Windows.Resources;
using System.Device.Location;
using System.Windows.Media;
using Microsoft.Phone.Maps.Toolkit;
using System.Threading.Tasks;
using System.IO;

namespace AppStudio.Views
{
    public partial class MyMap : PhoneApplicationPage
    {

        private static string _type;
        private static string _kmlName;
        
        public MyMap()
        {
            InitializeComponent();
            ProgressIndicator.Visibility = System.Windows.Visibility.Visible;
            _type = "no";
        }

       public ToursViewModel ToursViewModel { get; private set; }
       KmlContent KMLContent = new KmlContent();
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString["map"] != "ok") {
                ShowRoute();
            } else {
                _type = NavigationContext.QueryString["map"];
                ShowPosition();
            }
            base.OnNavigatedTo(e);
        }

        private void ShowPosition() {
            MapServices.ShowMyLocationOnTheMap(MainMap, ProgressIndicator);
            
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (PopupManager.IsOpen)
            {
                if (PopupManager.Instance.IsOpen)
                {
                    PopupManager.Instance.IsOpen = false;
                    PopupManager.IsOpen = false;
                    PopupManager.Instance = null;
                    e.Cancel = true;
                    return;
                } 
            }
            if (mapType.IsOpen)
            {
                mapType.IsOpen = false;
                e.Cancel = true;
                return;
            }
            base.OnBackKeyPress(e);
            MainMap.MapElements.Clear();
            MainMap.Layers.Clear();
            if (_type.Equals("ok")) {
                NavigationServices.NavigateToPage("MainPage");
            } else {
                NavigationServices.NavigateToPage("DetailView");
            }
            
        }

        private void FindMe_Click(object sender, EventArgs e)
        {
            MapServices.ShowMyLocationOnTheMap(this.MainMap, ProgressIndicator);
        }

        private void MapType_Click(object sender, EventArgs e)
        {
            if (mapType.IsOpen)
            {
                mapType.IsOpen = false;
            }
            else
            {
                mapType.IsOpen = true;
            }
        }
        private void CartoMode_Click(object sender, RoutedEventArgs e)
        {
            Button hit = sender as Button;
            switch (hit.Name)
            {
                case "btn_Hybrid":
                    MainMap.CartographicMode = Microsoft.Phone.Maps.Controls.MapCartographicMode.Hybrid;
                    break;
                case "btn_Road":
                    MainMap.CartographicMode = Microsoft.Phone.Maps.Controls.MapCartographicMode.Road;
                    break;
                case "btn_Aerial":
                    MainMap.CartographicMode = Microsoft.Phone.Maps.Controls.MapCartographicMode.Aerial;
                    break;
            }
        }
        private async void ShowRoute() {

            try {
                ToursViewModel = NavigationServices.CurrentViewModel as ToursViewModel;
                _kmlName = ToursViewModel.SelectedItem.Title;
                MapServices.DisplayRoute(MainMap, await KMLContent.ReadFiles(_kmlName), totalDistanceLabel);
            } catch (FileNotFoundException) {
                MessageBox.Show("Download this tour");
                NavigationServices.NavigateTo(new Uri("/Views/DetailView.xaml", UriKind.Relative));
            }

        }
        private void BackOnTrack_Click(object sender, EventArgs e)
        {
            if (_type == "ok") {
                MapServices.FindWayBack(MainMap, ProgressIndicator, false);
            } else {
                MapServices.FindWayBack(MainMap, ProgressIndicator, true);
            }
            
        }

        private void placesAround_Click(object sender, EventArgs e)
        {
            NavigationServices.NavigateTo(new Uri("/Views/PhotosAround.xaml", UriKind.Relative));
        }

        private void LocationTracker_Click(object sender, EventArgs e)
        {
            MainMap.Pitch = 55;
            MainMap.ZoomLevel = 17;
            MapServices.GPSLocator(MainMap, speedLabel, distanceLabel);
            GPSInfo.Visibility = System.Windows.Visibility.Visible;
        }

        private void MainMap_Loaded(object sender, RoutedEventArgs e)
        {
            ProgressIndicator.Visibility = System.Windows.Visibility.Collapsed;
            new MapRotationGesture(MainMap);
            new MapPitchGesture(MainMap);

            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "2e3cbb09-689a-425c-b85f-ae31738089ac";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "_UInuWssylYrNpdMIGGGDA";
        }
    }
}