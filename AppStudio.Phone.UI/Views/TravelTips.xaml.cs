﻿using System;
using System.Collections.Generic;
using Microsoft.Phone.Controls;
using AppStudio.Services;

namespace AppStudio.Views
{
    public partial class TravelTips : PhoneApplicationPage
    {
        private string _tbilisiTime;
        public string TbilisiTime 
        {
            get
            {
                TimeSpan localOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.Now);
                TimeSpan tbilisiOffset = new TimeSpan(4, 0, 0);
                TimeSpan timediff = tbilisiOffset - localOffset;
                DateTime tbilisiTime = DateTime.Now + timediff;
                _tbilisiTime = String.Format("{0}:{1}:{2}", tbilisiTime.Hour, tbilisiTime.Minute, tbilisiTime.Second);
                return _tbilisiTime;
            } 
            private set
            { 
                
            }
        }
        public TravelTips()
        {
            InitializeComponent();
            AddTips();
        }

        private void AddTips()
        {
            List<Tip> tipList = new List<Tip>();

            tipList.Add(new Tip()
            {
                TipTitle = "Safety First",
                TipBody = String.Format("Emergency Phone Numbers are:\n \u2022 Fire Department: 011\n \u2022 Police: 122\n \u2022 Ambulance: 033\n \u2022 Emergency Call Center: 112"),
                ImageUrl = @"/Assets/Tips/ems.png"
            });
            tipList.Add(new Tip()
            {
                TipTitle = "Georgia at Glance",
                TipBody = String.Format("Georgia is a country in the Caucasus region of Eurasia. Located at the crossroads of Western Asia and Eastern Europe, it is bounded to the west by the Black Sea, to the north by Russia, to the south by Turkey and Armenia, and to the southeast by Azerbaijan. The capital of Georgia is Tbilisi. Georgia covers a territory of 69,700 square kilometres (26,911 sq mi), and its population is almost 5 million. Georgia is a unitary, semi-presidential republic, with the government elected through a representative democracy. During the" +
                                        "classical era, independent kingdoms became established in what is now Georgia. The kingdoms of Colchis and Iberia adopted Christianity in the early 4th century. A unified Kingdom of Georgia reached the peak of its political and economic strength during the reign of King David IV and Queen Tamar in the 11th–12th centuries. After this the area was dominated by various large Empires, including the Safavids, Afsharids, and Qajar Persians. In the late 18th century the kingdom of Kartli-Kakheti forged an alliance with the"+
                                        "Russian Empire, and the area was annexed by Russia in 1801. After a brief period of independence following the Russian Revolution of 1917, Georgia was occupied by Soviet Russia in 1921, becoming the Georgian Soviet Socialist Republic and part of the Soviet Union. After independence in 1991, post-communist Georgia suffered from civil unrest and economic crisis for most of the 1990s. This lasted until the Rose Revolution of 2003, after which the new government introduced democratic and economic reforms.\n \u2022 Local Time:{0}\n \u2022 Local Currency: Lari", TbilisiTime),
                ImageUrl = @"/Assets/Tips/geo.png"
            });

            tipList.Add(new Tip() { TipTitle = "Transportation", 
                                    TipBody = String.Format("Schedules can be found:\n \u2022 Railway: www.railway.ge\n \u2022 Tbilisi Bus/Minibus/Subway: www.ttc.com.ge\n"
                                                                                          + "Transport between cities\nThere are couple minibus stations\n \u2022 Okriba - Addr.:Didube, Karaleti N4\n \u2022 Tbilisi Central Station Addr.:Ortachala, Gulia N1\n \u2022 Station Square Addr.:Station Square"),
                                    ImageUrl = @"/Assets/Tips/transport.png"
                        });
            tipList.Add(new Tip()
            {
                TipTitle = "Food",
                TipBody = "Georgian food is excellent, and people are very generous, so if you are invited to eat with someone, be prepared to get very full. Meals usually come with wine.Alcoholic drinks from Georgia include chacha and Georgian wine. Some of the most well-known Georgian wines include Pirosmani, Alazani, Akhasheni, Saperavi, and Kindzmarauli. Wine culture in Georgia dates back thousands of years, and many Georgian wines are made from traditional Georgian grape varieties that are little known in the West, such as Saperavi and Rkatsiteli.",
                                    ImageUrl = @"/Assets/Tips/food.png"
            });
            tipList.Add(new Tip()
            {
                TipTitle = "Music and Dance",
                TipBody = String.Format("Georgians love music and they have a varied history of folk music styles from different regions all over Georgia. The Georgians are masters of polyphony, a type of music that consists of two or more independent melodic voices. The term is usually used in reference to music of the late Middle Ages and Renaissance periods.\nThe Georgians are also renowned for their love of dance. There are many Georgian folk dances which are performed for a number of reasons:\nKartuli – This romantic dance is meant to remind the audience of a wedding.\nKhorumi – A war dance that originated in the Achara, the southwestern region of Georgia, only a few men originally performed Khorumi.\nKhevsuruli – This mountain dance best represents the Georgian spirit, uniting themes of love, courage, respect, skill, beauty and colorfulness in one incredible performance."),
                ImageUrl = @"/Assets/Tips/speakers.png"
            });
            DataContext = tipList;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationServices.NavigateToPage("MainPage");
        }
    }

    class Tip 
    {
        private string _ImageUrl = "/Assets/NoImage.png";
        public string TipTitle { get; set; }
        public string TipBody { get; set; }
        public string ImageUrl 
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; } 
        }
    }
}