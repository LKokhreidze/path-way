using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;
using AppStudio.Services;
using Microsoft.Phone.Controls;
using AppStudio.Views;
using AppStudio.Data;
using Windows.Storage;
using System.IO;

namespace AppStudio
{
    public partial class ImageViewer : PhoneApplicationPage
    {
        public ImageViewer()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (DetailView.IsImageClickAvailable) {
                NavigationService.Navigate(new Uri("/Views/MyMap.xaml?map=no", UriKind.Relative));
            } else {
               NavigationService.Navigate(new Uri("/Views/DetailView.xaml?error=er", UriKind.Relative));
            }
            base.OnNavigatedTo(e);
        }
    }
}
