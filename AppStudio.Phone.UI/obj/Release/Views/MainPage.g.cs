﻿#pragma checksum "C:\Users\Century\Documents\Discover Georgia\AppPhone\AppStudio.Phone.UI\Views\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6AB1918E6841DD5A0409A946C8372724"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Cimbalino.Phone.Toolkit.Behaviors;
using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace AppStudio {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Microsoft.Phone.Controls.Panorama Container;
        
        internal Microsoft.Phone.Controls.PanoramaItem MenuViewItem;
        
        internal Microsoft.Phone.Controls.PanoramaItem ToursViewItem;
        
        internal Microsoft.Phone.Controls.PanoramaItem MapViewItem;
        
        internal Microsoft.Phone.Controls.PanoramaItem InfoViewItem;
        
        internal Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarBehavior appBar;
        
        internal Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarIconButton btn_refresh;
        
        internal Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarIconButton btn_send;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/AppStudio;component/Views/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.Container = ((Microsoft.Phone.Controls.Panorama)(this.FindName("Container")));
            this.MenuViewItem = ((Microsoft.Phone.Controls.PanoramaItem)(this.FindName("MenuViewItem")));
            this.ToursViewItem = ((Microsoft.Phone.Controls.PanoramaItem)(this.FindName("ToursViewItem")));
            this.MapViewItem = ((Microsoft.Phone.Controls.PanoramaItem)(this.FindName("MapViewItem")));
            this.InfoViewItem = ((Microsoft.Phone.Controls.PanoramaItem)(this.FindName("InfoViewItem")));
            this.appBar = ((Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarBehavior)(this.FindName("appBar")));
            this.btn_refresh = ((Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarIconButton)(this.FindName("btn_refresh")));
            this.btn_send = ((Cimbalino.Phone.Toolkit.Behaviors.ApplicationBarIconButton)(this.FindName("btn_send")));
        }
    }
}

