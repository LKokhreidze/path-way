using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;
using AppStudio.Services;
using Microsoft.Phone.Controls;
using AppStudio.Views;
using AppStudio.Data;
using Windows.Storage;
using System.IO;
namespace AppStudio.Data
{
    public class MenuViewModel : ViewModelBase<MenuSchema>
    {
        override protected string CacheKey
        {
            get { return "Menu1DataSource"; }
        }

        override protected IDataSource<MenuSchema> CreateDataSource()
        {
            return new Menu1DataSource(); // MenuDataSource
        }

        override public bool IsStaticData
        {
            get { return true; }
        }

        override protected void NavigateToSelectedItem()
        {
            var currentItem = GetCurrentItem();
            if (currentItem != null)
            {
                if (currentItem.GetValue("Type").EqualNoCase("Section"))
                {
                    NavigationServices.NavigateToPage(currentItem.GetValue("Target"));
                }
                else
                {
                    NavigationServices.NavigateTo(new Uri(currentItem.GetValue("Target"), UriKind.Relative));
                }
            }
        }
    }
}
