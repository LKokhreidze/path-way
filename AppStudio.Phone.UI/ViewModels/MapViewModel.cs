using System;
using System.Windows;
using System.Windows.Input;

using AppStudio.Services;

namespace AppStudio.Data
{
    public class MapViewModel : ViewModelBase<MapSchema>
    {
        override protected string CacheKey
        {
            get { return "MapDataSource"; }
        }

        override protected IDataSource<MapSchema> CreateDataSource()
        {
            return new MapDataSource(); // StaticDataSource
        }

        override public bool IsStaticData
        {
            get { return true; }
        }

        override public bool IsNewRefreshVisible
        {
            get { return true; }
        }
    }
}
