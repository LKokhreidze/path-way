using System;
using System.Windows;
using System.Windows.Input;

using AppStudio.Services;

namespace AppStudio.Data
{
    public class InfoViewModel : ViewModelBase<HtmlSchema>
    {
        override protected string CacheKey
        {
            get { return "InfoDataSource"; }
        }

        override protected IDataSource<HtmlSchema> CreateDataSource()
        {
            return new InfoDataSource(); // HtmlDataSource
        }

        override public bool IsStaticData
        {
            get { return true; }
        }

        public override bool IsSendVisible {
            get {
               return true;
            }
        }
    }
}
