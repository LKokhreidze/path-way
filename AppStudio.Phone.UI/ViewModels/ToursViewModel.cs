using System;
using System.Windows;
using System.Windows.Input;

using AppStudio.Services;

namespace AppStudio.Data
{
    public class ToursViewModel : ViewModelBase<ToursSchema>
    {
        override protected string CacheKey
        {
            get { return "ToursDataSource"; }
        }

        override protected IDataSource<ToursSchema> CreateDataSource()
        {
            return new ToursDataSource(); // StaticDataSource
        }

        override public bool IsStaticData
        {
            get { return true; }
        }

        override public bool IsPinToStartVisible
        {
            get { return ViewType == ViewTypes.Detail; }
        }

        override public void PinToStart()
        {
            base.PinToStart("DetailView", "{DefaultTitle}", "{DefaultSummary}", "{DefaultImageUrl}");
        }

        override public bool IsShareItemVisible
        {
            get { return ViewType == ViewTypes.Detail; }
        }
        
        override public void ShareItem()
        {
            base.ShareItem("{DefaultTitle}", "{DefaultSummary}", "{DefaultImageUrl}", "{DefaultImageUrl}");
        }

        override public bool IsSpeakTextVisible
        {
            get { return ViewType == ViewTypes.Detail; }
        }
        

        override public bool IsRefreshVisible
        {
            get { return ViewType == ViewTypes.List; }
        }

        public override bool IsSearchVisible {
            get {
                return true;
            }
        }
        public static bool IsImageClickAvailable { get; set; }
        //public override ICommand ImageSelectedCommand {
        //    get {
        //        if (DetailView.IsImageClickAvailable == false) {
        //            return null;
                    
        //        } else {
        //            return base.ImageSelectedCommand;
        //        }
        //    }
        //}

        override protected void NavigateToSelectedItem()
        {
            NavigationServices.NavigateToPage("DetailView");
        }
    }
}
