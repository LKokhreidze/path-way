using System.Threading.Tasks;
using System.Windows.Input;

using AppStudio.Data;
using AppStudio.Services;

namespace AppStudio
{
    public class MainViewModels : BindableBase
    {
       private MenuViewModel _menuViewModel;
       private ToursViewModel _toursViewModel;
       private MapViewModel _mapViewModel;
       private InfoViewModel _infoViewModel;

        private ViewModelBase _selectedItem = null;

        public MainViewModels()
        {
            _selectedItem = MenuViewModel;
        }
 
        public MenuViewModel MenuViewModel
        {
            get { return _menuViewModel ?? (_menuViewModel = new MenuViewModel()); }
        }
 
        public ToursViewModel ToursViewModel
        {
            get { return _toursViewModel ?? (_toursViewModel = new ToursViewModel()); }
        }
 
        public MapViewModel MapViewModel
        {
            get { return _mapViewModel ?? (_mapViewModel = new MapViewModel()); }
        }
 
        public InfoViewModel InfoViewModel
        {
            get { return _infoViewModel ?? (_infoViewModel = new InfoViewModel()); }
        }

        public void SetViewType(ViewTypes viewType)
        {
            MenuViewModel.ViewType = viewType;
            ToursViewModel.ViewType = viewType;
            MapViewModel.ViewType = viewType;
            InfoViewModel.ViewType = viewType;
        }

        public ViewModelBase SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
                UpdateAppBar();
            }
        }

        public bool IsAppBarVisible
        {
            get
            {
                
                    return true;
            }
        }

        public bool IsLockScreenVisible
        {
            get { return SelectedItem == null || SelectedItem == MenuViewModel; }
        }

        public bool IsAboutVisible
        {
            get { return SelectedItem == null || SelectedItem == MenuViewModel; }
        }

        public void UpdateAppBar()
        {
            OnPropertyChanged("IsAppBarVisible");
            OnPropertyChanged("IsLockScreenVisible");
            OnPropertyChanged("IsAboutVisible");
        }

        /// <summary>
        /// Load ViewModel items asynchronous
        /// </summary>
        public async Task LoadData(bool isNetworkAvailable)
        {
            var loadTasks = new Task[]
            { 
                MenuViewModel.LoadItems(isNetworkAvailable),
                ToursViewModel.LoadItems(isNetworkAvailable),
                MapViewModel.LoadItems(isNetworkAvailable),
                InfoViewModel.LoadItems(isNetworkAvailable),
            };
            await Task.WhenAll(loadTasks);
        }

        //
        //  ViewModel command implementation
        //
        public ICommand AboutCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    NavigationServices.NavigateToPage("AboutThisAppPage");
                });
            }
        }

        public ICommand LockScreenCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    LockScreenServices.SetLockScreen("LockScreenImage.jpg");
                });
            }
        }
    }
}
