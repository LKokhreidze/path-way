﻿
namespace AppStudio.Services
{
    enum MapSymbol
    {
        Airport = 10,
        Archeology = 11,
        Bank = 12,
        Bridge = 13,
        Bus = 14,
        Camp = 15,
        Castle = 16,
        Church = 17,
        Event = 18,
        Fishing = 19,
        Hospital = 20,
        Hotel = 21,
        Info = 22,
        Monument = 23,
        Mosque = 24,
        Museum = 25,
        Nature = 26,
        Park = 27,
        Parking = 28,
        Picnic = 29,
        Police = 30,
        Resraurant = 31,
        Synagogue = 32,
        Train = 33,
        View = 34,
        WC = 35,
        Wine = 36
    }
}
