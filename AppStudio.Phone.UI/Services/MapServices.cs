﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using System.Windows;
using Windows.System;
using Microsoft.Phone.Maps.Services;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Kml;
using Microsoft.Phone.Maps.Toolkit;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Windows.Controls;
using MobileBearing;

namespace AppStudio.Services
{
    public class MapServices : IDisposable
    {
        private static GeoCoordinateWatcher _watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
        private static MapPolyline _line;
        private static BitmapImage image;
        private static List<Pushpin> pins;
        private static double _kilometres;
        private static double kilometers;
        private static GeoCoordinate MyCoordinate = null;
        private static ReverseGeocodeQuery MyReverseGeocodeQuery = null;
        private static double _accuracy = 0.0;

        static public bool IsLocationServiceEnabled
        {
            get
            {
                Geolocator locationservice = new Geolocator();
                if (locationservice.LocationStatus == PositionStatus.Disabled)
                {
                    return false;
                }
                return true;
            }
        }

        public static void GPSLocator(Map MainMap, TextBlock speedLabel, TextBlock distanceLabel)
        {
            if (_watcher != null)
            {
                _line = new MapPolyline();
                _line.StrokeColor = Colors.Green;
                _line.StrokeThickness = 5;
                MainMap.MapElements.Add(_line);
                _watcher.PositionChanged += (sender, e) => _watcher_PositionChanged(sender, e, MainMap, speedLabel, distanceLabel);
            }
            _watcher.Start();

        }

        private static void _watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e, Map MainMap, TextBlock speedLabel, TextBlock distanceLabel)
        {
            var coord = new GeoCoordinate(e.Position.Location.Latitude, e.Position.Location.Longitude);
            if (_line.Path.Count > 0)
            {
                //წინა წერტილის პოვნა
                var previousPoint = _line.Path.Last();
                //შემდეგ წერტილსა და წინა წერტილს შორის მანძილის გაზომვა
                var distance = coord.GetDistanceTo(previousPoint);



                // და მანძილის გაზომვა
                _kilometres += distance / 1000.0;

                speedLabel.Text = (e.Position.Location.Speed * 3.5).ToString();
                distanceLabel.Text = string.Format("{0:f2} km", _kilometres);

                PositionHandler handler = new PositionHandler();
                var heading = handler.CalculateBearing(new Position(previousPoint), new Position(coord));
                MainMap.SetView(coord, MainMap.ZoomLevel, heading, MapAnimationKind.Parabolic);

                //ShellTile.ActiveTiles.First().Update(new IconicTileData())
                //{

                //}
            }
            else
            {
                MainMap.Center = coord;
            }
            #region MyRegion
            //if (MainMap.Layers != null)
            //{
            //    if (MainMap.Layers.Count != 0)
            //    {
            //        var pushpin = MainMap.Layers.FirstOrDefault(p => (p.GetType() == typeof(MapLayer) && ((MapLayer)p).FirstOrDefault(s => (s.GetType() == typeof(MapOverlay) && ((Pushpin)(((MapOverlay)s).Content)).Tag == "locationPushpin")) != null));
            //        if (pushpin != null)
            //        {
            //            MainMap.Layers.Remove(pushpin);
            //        }
            //    }

            //}

            //MainMap.Center = coord;
            //Pushpin currentlocation = new Pushpin();
            //currentlocation.Style = Application.Current.Resources["PushpinStyle"] as System.Windows.Style;
            //currentlocation.GeoCoordinate = coord;
            //currentlocation.Tag = "currentLocation";
            //MapOverlay currLocationOverlay = new MapOverlay();
            //currLocationOverlay.Content = currentlocation;
            //currLocationOverlay.PositionOrigin = new System.Windows.Point(0.5, 1.0);
            //currLocationOverlay.GeoCoordinate = coord;
            //MapLayer currLocationLayer = new MapLayer();
            //currLocationLayer.Add(currLocationOverlay);
            //MainMap.Layers.Add(currLocationLayer); 
            #endregion
            _line.Path.Add(coord);

        }
        static public void MapPosition(string address)
        {
            var geoQ = new GeocodeQuery();

            if (geoQ != null)
            {
                geoQ.GeoCoordinate = new GeoCoordinate(0, 0);
                geoQ.SearchTerm = address;
                geoQ.QueryCompleted += GeoQ_MapPositionQueryCompleted;
                geoQ.QueryAsync();
            }
        }

        static public void HowToGet(string address)
        {
            var geoQ = new GeocodeQuery();

            if (geoQ != null)
            {
                geoQ.GeoCoordinate = new GeoCoordinate(0, 0);
                geoQ.SearchTerm = address;
                geoQ.QueryCompleted += GeoQ_HowToGetQueryCompleted;
                geoQ.QueryAsync();
            }
        }

        static private async void GeoQ_MapPositionQueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Result.Any())
            {
                var result = e.Result.First();

                var latitude = result.GeoCoordinate.Latitude.ToString(CultureInfo.InvariantCulture);
                var longitude = result.GeoCoordinate.Longitude.ToString(CultureInfo.InvariantCulture);

                var uri = string.Concat("explore-maps://v2.0/show/place/?latlon=", latitude, ",", longitude);

                await Launcher.LaunchUriAsync(new Uri(uri));
            }
            else if (e.UserState is GeocodeQuery)
            {
                MessageBox.Show("No results for " + (e.UserState as GeocodeQuery).SearchTerm);
            }
        }

        static private async void GeoQ_HowToGetQueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Result.Any())
            {
                var result = e.Result.First();

                var latitude = result.GeoCoordinate.Latitude.ToString(CultureInfo.InvariantCulture);
                var longitude = result.GeoCoordinate.Longitude.ToString(CultureInfo.InvariantCulture);
                var uri = string.Concat("directions://v2.0/route/destination/?latlon=", latitude, ",", longitude);

                await Launcher.LaunchUriAsync(new Uri(uri));
            }
            else if (e.UserState is GeocodeQuery)
            {
                MessageBox.Show("No results for " + (e.UserState as GeocodeQuery).SearchTerm);
            }
        }

        static private GeoCoordinate ConvertGeocoordinate(Geocoordinate geocoordinate)
        {
            return new GeoCoordinate
                (
                geocoordinate.Latitude,
                geocoordinate.Longitude,
                geocoordinate.Altitude ?? Double.NaN,
                geocoordinate.Accuracy,
                geocoordinate.AltitudeAccuracy ?? Double.NaN,
                geocoordinate.Speed ?? Double.NaN,
                geocoordinate.Heading ?? Double.NaN
                );
        }

        static public async void FindWayBack(Map mainMap, ProgressBar indicator, bool IsRoute)
        {
            if (IsRoute == true) {
                indicator.Visibility = Visibility.Visible;
                ShowMyLocationOnTheMap(mainMap, indicator);
                Geocoordinate myGeocoordinate = await AppStudio.Data.PositionDataProvider.MyPosition();
                GeoCoordinate myGeoCoordinate = ConvertGeocoordinate(myGeocoordinate);
                Double min = Math.Sqrt(Math.Pow((myGeoCoordinate.Longitude - pins[0].GeoCoordinate.Longitude), 2) + Math.Pow((myGeoCoordinate.Longitude - pins[0].GeoCoordinate.Longitude), 2));
                int minIndex = 0;
                for (int i = 0; i < pins.Count; i++) {
                    Double current = Math.Sqrt(Math.Pow((myGeoCoordinate.Longitude - pins[i].GeoCoordinate.Longitude), 2) + Math.Pow((myGeoCoordinate.Latitude - pins[i].GeoCoordinate.Longitude), 2));
                    if (min > current) {
                        min = current;
                        minIndex = i;
                    }
                }
                List<GeoCoordinate> backWayPoints = new List<GeoCoordinate>();
                backWayPoints.Add(myGeoCoordinate);
                backWayPoints.Add(pins[minIndex].GeoCoordinate);
                RouteQuery backOnTrack = new RouteQuery();
                backOnTrack.QueryCompleted += (sender, e) => backOnTrack_QueryCompleted(sender, e, mainMap, indicator);
                backOnTrack.Waypoints = backWayPoints;
                backOnTrack.QueryAsync();
            } else {
                MessageBox.Show("There is no track on the map");
            }
           

        }

        static public async void GetCurrentLocationAdress()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracy = PositionAccuracy.High;

            try
            {
                Geoposition currentPosition = await geolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                _accuracy = currentPosition.Coordinate.Accuracy;

                MyCoordinate = new GeoCoordinate(currentPosition.Coordinate.Latitude, currentPosition.Coordinate.Longitude);

                if (MyReverseGeocodeQuery == null || !MyReverseGeocodeQuery.IsBusy)
                {
                    MyReverseGeocodeQuery = new ReverseGeocodeQuery();
                    MyReverseGeocodeQuery.GeoCoordinate = new GeoCoordinate(MyCoordinate.Latitude, MyCoordinate.Longitude);
                    MyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
                    MyReverseGeocodeQuery.QueryAsync();
                }

            }
            catch (Exception ex)
            {
                AppLogs.WriteError(ex.Message, ex);
            }

        }

        private static void ReverseGeocodeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    MapAddress address = e.Result[0].Information.Address;
                    
                }
            }
        }

        private static void backOnTrack_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e, Map mainMap, ProgressBar indicator)
        {
            if (null == e.Error)
            {
                Route MyRoute = e.Result;
                MapRoute mappedRoute = new MapRoute(MyRoute);
                mainMap.AddRoute(mappedRoute);
                mainMap.SetView(mappedRoute.Route.BoundingBox);
                indicator.Visibility = Visibility.Collapsed;
            }
        }
        private static System.Windows.Style SetStyles(int symbolNum)
        {
            MapSymbol symblos = (MapSymbol)symbolNum;
            switch (symblos)
            {
                case MapSymbol.Airport:
                    return Application.Current.Resources["Airport"] as System.Windows.Style;
                case MapSymbol.Archeology:
                    return Application.Current.Resources["Archeology"] as System.Windows.Style;
                case MapSymbol.Bank:
                    return Application.Current.Resources["Bank"] as System.Windows.Style;
                case MapSymbol.Bridge:
                    return Application.Current.Resources["Bridge"] as System.Windows.Style;
                case MapSymbol.Bus:
                    return Application.Current.Resources["Bus"] as System.Windows.Style;
                case MapSymbol.Camp:
                    return Application.Current.Resources["Camp"] as System.Windows.Style;
                case MapSymbol.Castle:
                    return Application.Current.Resources["Castle"] as System.Windows.Style;
                case MapSymbol.Church:
                    return Application.Current.Resources["Church"] as System.Windows.Style;
                case MapSymbol.Event:
                    return Application.Current.Resources["Event"] as System.Windows.Style;
                case MapSymbol.Fishing:
                    return Application.Current.Resources["Fishing"] as System.Windows.Style;
                case MapSymbol.Hospital:
                    return Application.Current.Resources["Hospital"] as System.Windows.Style;
                case MapSymbol.Hotel:
                    return Application.Current.Resources["Hotel"] as System.Windows.Style;
                case MapSymbol.Info:
                    return Application.Current.Resources["Info"] as System.Windows.Style;
                case MapSymbol.Monument:
                    return Application.Current.Resources["Monument"] as System.Windows.Style;
                case MapSymbol.Mosque:
                    return Application.Current.Resources["Mosque"] as System.Windows.Style;
                case MapSymbol.Museum:
                    return Application.Current.Resources["Museum"] as System.Windows.Style;
                case MapSymbol.Nature:
                    return Application.Current.Resources["Nature"] as System.Windows.Style;
                case MapSymbol.Park:
                    return Application.Current.Resources["Park"] as System.Windows.Style;
                case MapSymbol.Parking:
                    return Application.Current.Resources["Parking"] as System.Windows.Style;
                case MapSymbol.Picnic:
                    return Application.Current.Resources["Picnic"] as System.Windows.Style;
                case MapSymbol.Police:
                    return Application.Current.Resources["Police"] as System.Windows.Style;
                case MapSymbol.Resraurant:
                    return Application.Current.Resources["Resraurant"] as System.Windows.Style;
                case MapSymbol.Synagogue:
                    return Application.Current.Resources["Synagogue"] as System.Windows.Style;
                case MapSymbol.Train:
                    return Application.Current.Resources["Train"] as System.Windows.Style;
                case MapSymbol.View:
                    return Application.Current.Resources["View"] as System.Windows.Style;
                case MapSymbol.WC:
                    return Application.Current.Resources["WC"] as System.Windows.Style;
                case MapSymbol.Wine:
                    return Application.Current.Resources["Wine"] as System.Windows.Style;
            }
            return null;
        }
        static public async void ShowMyLocationOnTheMap(Map mainMap, ProgressBar indicator)
        {
            indicator.Visibility = Visibility.Visible;
            //ვადგენთ ადგილმდებარეობას

            Geocoordinate myGeocoordinate = await AppStudio.Data.PositionDataProvider.MyPosition();
            GeoCoordinate myGeoCoordinate = ConvertGeocoordinate(myGeocoordinate);

            //რუკაზე კოორდინატების ასახვა
            mainMap.Center = myGeoCoordinate;
            mainMap.ZoomLevel = 13;

            Pushpin myLocation = new Pushpin();
            myLocation.Style = Application.Current.Resources["PushpinStyle"] as System.Windows.Style;
            myLocation.GeoCoordinate = myGeoCoordinate;
            //რუკის ფენის მომზადება
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myLocation;
            myLocationOverlay.PositionOrigin = new System.Windows.Point(0.5, 1.0);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;

            //ფენის შექმნა
            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);
            //ფენის დამატება
            mainMap.Layers.Add(myLocationLayer);
            indicator.Visibility = Visibility.Collapsed;
        }

        static public void DisplayRoute(Map MainMap, kml Data, TextBlock totalDistanceLabel)
        {
            KmlContent content = new KmlContent();
            pins = new List<Pushpin>();
            MapLayer pinsLayer = new MapLayer();
            kilometers = 0;

            for (int i = 0; i < Data.Document.Placemarks.Count; i++)
            {

                if (Data.Document.Placemarks[i].LineString != null)
                {

                    MapPolyline line = new MapPolyline();
                    line.StrokeColor = Colors.Blue;
                    line.StrokeThickness = 5;


                    foreach (GeoCoordinate item in content.ParseLocation(Data.Document.Placemarks[i].LineString.Coordinates))
                    {
                        if (line.Path.Count > 0)
                        {
                            var previousCoord = line.Path.Last();
                            var distance = item.GetDistanceTo(previousCoord);
                            kilometers += distance / 1000.0;
                            totalDistanceLabel.Text = string.Format("{0:f2} km", kilometers);
                        }
                        line.Path.Add(item);
                    }

                    MainMap.MapElements.Add(line);
                }

                if (Data.Document.Placemarks[i].Point != null)
                {
                    image = new BitmapImage();
                    var pinInfo = Data.Document.Placemarks[i];
                    //var styles = from s in Data.Document.Styles
                    //             where s.Id == pinInfo.StyleUrl
                    //             select s;
                    Pushpin point = new Pushpin();
                    point.GeoCoordinate = content.ParseLocation(pinInfo.Point.Coordinates)[0];
                    point.DataContext = pinInfo.Description;
                    point.Content = pinInfo.Name;
                    point.Tap += pinTap_Event;
                    point.Style = SetStyles(Int32.Parse(pinInfo.StyleUrl));
                    pins.Add(point);
                    MapOverlay pin = new MapOverlay();
                    pin.Content = point;
                    pin.PositionOrigin = new System.Windows.Point(0.5, 1.0);
                    pin.GeoCoordinate = point.GeoCoordinate;


                    pinsLayer.Add(pin);
                }
            }

            string centerCoordinate = (from coord in Data.Document.Placemarks
                                       where coord.LineString != null
                                       select coord.LineString.Coordinates).First().ToString();

            MainMap.Layers.Add(pinsLayer);
            MainMap.Center = content.ParseLocation(centerCoordinate)[0];
            MainMap.ZoomLevel = 12;

        }

        static private void pinTap_Event(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pushpin pin = sender as Pushpin;

            string trimed = HtmlUtil.CleanHtml(pin.DataContext.ToString());

            image.UriSource = new Uri(ParseForImage(pin.DataContext.ToString()), UriKind.RelativeOrAbsolute);

            PopupManager.PlaceImage = image;
            PopupManager.PlaceInfo = Regex.Replace(trimed, "&nbsp;", " ");
            PopupManager.Instance.IsOpen = true;
        }

        static private string ParseForImage(string data)
        {
            string imgUri = Regex.Match(data, "<img.+?src=\"(.+?)\".+?/?>").Groups[1].Value;
            if (imgUri.Equals(String.Empty))
            {
                return "Assets/DataImages/NoImage.png";
            }
            else
            {
                return imgUri;
            }

        }
        void IDisposable.Dispose()
        {
            _watcher = null;
            kilometers = 0;
            MyCoordinate = null;
            MyReverseGeocodeQuery = null;
        }
    }
}
