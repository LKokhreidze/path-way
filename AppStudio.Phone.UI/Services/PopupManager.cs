﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AppStudio.Services
{
    class PopupManager
    {
        private static bool _isOpen = false;

        public static bool IsOpen { get { return _isOpen; } set{_isOpen = value;} }
        public static  Popup Instance
        {
            get { return _popup ?? (_popup = GeneratePopup()); }
            set { _popup = value; }
        } private static Popup _popup;
        public static ImageSource PlaceImage { get; set; }
        public static string PlaceInfo { get; set; }
        static private Popup  GeneratePopup()
        {
            _isOpen = true;
            Popup popUp = new Popup();
            Border border = new Border();
            StackPanel headerStack = new StackPanel();
            Border infoBorder = new Border();
            StackPanel infoStack = new StackPanel();
            ScrollViewer verticalScrollInfo = new ScrollViewer();
            verticalScrollInfo.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            verticalScrollInfo.Height = 400;

            border.BorderThickness = new Thickness(2);
            border.Background = (SolidColorBrush)Application.Current.Resources["PhoneSemitransparentBrush"];
            border.Width = 479;

            TextBlock headerText = new TextBlock();
            headerText.Foreground = new SolidColorBrush(Colors.White);
            headerText.Text = "Place Info";
            headerText.FontSize = 28;
            headerText.Margin = new Thickness(5, 5, 0, 5);

            infoStack.VerticalAlignment = VerticalAlignment.Stretch;
            infoStack.Width = 460;
            infoStack.Height = 711;
            Image placeImage = new Image();
            placeImage.Source = PlaceImage;
            placeImage.Height = 250;
            placeImage.Width = 300;
            placeImage.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            placeImage.VerticalAlignment = System.Windows.VerticalAlignment.Center;

            TextBlock placeInfo = new TextBlock();
            placeInfo.Foreground = new SolidColorBrush(Colors.White);
            placeInfo.Text = PlaceInfo;
            placeInfo.TextWrapping = TextWrapping.Wrap;
            placeInfo.FontSize = 24;
            placeInfo.MinHeight = 1200;
            verticalScrollInfo.Content = placeInfo;
            

            infoStack.Children.Add(placeImage);
            infoStack.Children.Add(verticalScrollInfo);

            headerStack.Children.Add(headerText);
            headerStack.Children.Add(infoStack);
            border.Child = headerStack;
            popUp.Child = border;

            return popUp;
        }
    }
}