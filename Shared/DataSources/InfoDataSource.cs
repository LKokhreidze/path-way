using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppStudio.Data
{
    public class InfoDataSource : IDataSource<HtmlSchema>
    {
        //private static IEnumerable<HtmlSchema> _data;
        //public void Method()
        //{
        //    List<string> lst = new List<string>();
        //    lst.Add("first");
        //    lst.Add("second");
        //    foreach (var item in lst)
        //    {
        //        _data = new HtmlSchema[] {
        //            new HtmlSchema {
        //                 Id = "{2a968f78-86a0-4be2-a6ca-201d3f13ae73}",
        //                 Content = item,
                    
        //            }
        //        };


        //    }
        //}

        private IEnumerable<HtmlSchema> _data = new HtmlSchema[]
        {
            new HtmlSchema
            {
                Id = "{2a968f78-86a0-4be2-a6ca-201d3f13ae73}",
            }
        };

        public async Task<IEnumerable<HtmlSchema>> LoadData()
        {
            //Method();
            return await Task.Run(() =>
            {
                return _data;
            });
        }

        public async Task<IEnumerable<HtmlSchema>> Refresh()
        {
            return await LoadData();
        }
    }
}
