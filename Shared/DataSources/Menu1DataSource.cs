using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppStudio.Data
{
    public class Menu1DataSource : IDataSource<MenuSchema>
    {
        private readonly IEnumerable<MenuSchema> _data = new MenuSchema[]
		{
            new MenuSchema
            {
                Id = "{00000000-0000-0000-0000-000000000000}",
                Type = "Url",
                Title = "Look Around",
                Icon = "/Assets/DataImages/lookaround.png",
                Target = "",
            },
            new MenuSchema
            {
                Id = "{00000000-0000-0000-0000-000000000000}",
                Type = "Url",
                Title = "Cuisine",
                Icon = "/Assets/DataImages/cousin.png",
                Target = "",
            },
            new MenuSchema
            {
                Id = "{00000000-0000-0000-0000-000000000000}",
                Type = "Url",
                Title = "Tips",
                Icon = "/Assets/DataImages/tip_dark.png",
                Target = "",
            },
            new MenuSchema
            {
                Id = "{00000000-0000-0000-0000-000000000000}",
                Type = "Url",
                Title = "Map",
                Icon = "/Assets/DataImages/Map.png",
                Target = "",
            },
		};

        public async Task<IEnumerable<MenuSchema>> LoadData()
        {
            //GetFile();
            return await Task.Run(() =>
            {
                return _data;
            });
        }

        public async Task<IEnumerable<MenuSchema>> Refresh()
        {
            return await LoadData();
        }

        //private StorageFolder _local = ApplicationData.Current.LocalFolder;
        //private StorageFolder _localFolder;
        //private StorageFile _xmlFile;
        //private TourList.TourData _localTours;

        //private void GetFile() {
        //    WebClient client = new WebClient();
        //    client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_CheckXml);
        //    client.OpenReadAsync(new Uri("http://lamerwebmaster.dsl.ge/developer/TourInfo.xml", UriKind.Absolute));
        //}

        //private async void client_CheckXml(object sender, OpenReadCompletedEventArgs e) {
        //    _localFolder = await _local.CreateFolderAsync("ToursFolder", CreationCollisionOption.OpenIfExists);
        //    _xmlFile = await _localFolder.CreateFileAsync("Tours.xml", CreationCollisionOption.OpenIfExists);
        //    TourList.TourData xmldoc = XmlDeserialization(e.Result);
        //    if (new FileInfo(_xmlFile.Path).Length != 0) {
        //        await FindDifferences(xmldoc);
        //    } else {
        //        WriteInLocalFile(xmldoc);
        //    }
        //}

        //private TourList.TourData XmlDeserialization(Stream content) {
        //    using (TextReader reader = new StreamReader(content)) {
        //        XmlSerializer deserializer = new XmlSerializer(typeof(TourList.TourData));
        //        object obj = deserializer.Deserialize(reader);
        //        TourList.TourData KmlData = (TourList.TourData)obj;
        //        return KmlData;
        //    }
        //}

        //private void WriteInLocalFile(TourList.TourData source) {
        //    XmlSerializer serializer = new XmlSerializer(typeof(TourList.TourData));
        //    using (FileStream s = File.Create(_xmlFile.Path)) {
        //        serializer.Serialize(s, source);
        //    }
        //    _localTours = source;
        //}

        //private async Task FindDifferences(TourList.TourData source) {
        //    if (_local != null) {
        //        using (Stream fileToRead = await _localFolder.OpenStreamForReadAsync("Tours.xml")) {
        //            _localTours = XmlDeserialization(fileToRead);
        //            IEnumerable<TourList.Tour> dif = source.Tour.Except(_localTours.Tour, new TourList.TourComparer()).ToList();
        //            if (dif != null) {
        //                _localTours.Tour.AddRange(dif);
        //                WriteInLocalFile(_localTours);
        //            }
        //        }
        //    }
        //}

        //public static Task<Stream> DownloadFile(Uri url) {
        //    var tcs = new TaskCompletionSource<Stream>();
        //    var wc = new WebClient();
        //    wc.OpenReadCompleted += (s, e) => {
        //        if (e.Error != null) {
        //            tcs.TrySetException(e.Error);
        //        } else if (e.Cancelled) {
        //            tcs.TrySetCanceled();
        //        } else {
        //            tcs.TrySetResult(e.Result);
        //        }
        //    };
        //    wc.OpenReadAsync(url);
        //    return tcs.Task;
        //}

        //public async Task WriteDownloadedFiles(Uri uriToDownload, string fileName) {
        //    try {
        //        using (Stream myStr = await DownloadFile(uriToDownload)) {
        //            StorageFile storageFile = await _localFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
        //            using (Stream outputStream = await storageFile.OpenStreamForWriteAsync()) {
        //                await myStr.CopyToAsync(outputStream);
        //            }
        //        }
        //    } catch (Exception exc) {
        //        MessageBox.Show(exc.Message);
        //    }
        //}

        //private async void DownloadKML(string url, string name) {
        //    await WriteDownloadedFiles(new Uri(url, UriKind.Absolute), name);
        //}

        //private void Button_Click_1(object sender, RoutedEventArgs e) {
        //    foreach (var item in _localTours.Tour) {
        //        DownloadKML(item.TourUrl, item.Name);
        //    }
        //}

        //private void LayoutRoot_Loaded(object sender, RoutedEventArgs e) {

        //}


    }
}
