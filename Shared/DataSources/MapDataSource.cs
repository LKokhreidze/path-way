using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace AppStudio.Data {
    public class MapDataSource : IDataSource<MapSchema> {
        private static IEnumerable<MapSchema> _hotelData = new MapSchema[]
		{
            new MapSchema {
                Id = "1",
                Title = "Holiday Inn",
                Address = "Rustavel 17", 
                Latitude =  44.777939,
                Longtitude = 41.718921,
                Price = 145,
                Icon = "/Assets/Hotels/holiday.png",
            },
            new MapSchema {
                Id = "2",
                Title = "Tbilisi Marriott",
                Address = "Rustavel 17", 
                Latitude =  44.795660,
                Longtitude = 41.700604,
                Price = 145,
                Icon = "/Assets/Hotels/marriott.png",
            },
            new MapSchema {
                Id = "3",               
                Title = "Betsy's Hotel",
                Address = "32-34 Kote Makashvili Street, Tbilisi",
                Latitude =  44.786929,
                Longtitude = 41.701574,
                Price = 145,
                Icon = "/Assets/Hotels/batsy.png",
            }
            ,
            new MapSchema {
                Id = "4",               
                Title = "Hotel Vedzisi",
                Address = "32 Zovreti street, Tbilisi",
                Latitude =44.763135,
                Longtitude =  41.732483,
                Price = 140,
                Icon = "/Assets/Hotels/vedzisi.png",
            },
            new MapSchema {
                Id = "5",               
                Title = "Old Tiflis",
                Address = "3/9 Grishashvili Street, Tbilisi",
                Latitude = 44.817661,
                Longtitude = 41.686307,
                Price = 90,
                Icon = "/Assets/Hotels/tiflis.png",
            }
            ,
            new MapSchema {
                Id = "6",               
                Title = "Babilina",
                Address = "31 Shota Nishnianidze Street, Tbilisi",
                Latitude = 44.804439,
                Longtitude = 41.696025, 
                Price = 50,
                Icon = "/Assets/Hotels/babilina.png",
            },
             new MapSchema {
                Id = "7",               
                Title = "Hotel Kopala",
                Address = "8/10 Chekhov Street, Tbilisi",
                Latitude = 44.812712,
                Longtitude = 41.690967, 
                Price = 45,
                Icon = "/Assets/Hotels/kopala.png",
            },
             new MapSchema {
                Id = "8",               
                Title = "Hotel Old Metekhi",
                Address = "3 Metekhi Street, Tbilisi",
                Latitude = 44.813799,
                Longtitude = 41.690535, 
                Price = 65,
                Icon = "/Assets/Hotels/old.png",
            },
             new MapSchema {
                Id = "9",               
                Title = "Hotel Epic",
                Address = "Avlabari, Samreklo (Uritski) Street 26, Tbilisi",
                Latitude = 44.844574,
                Longtitude = 41.691504, 
                Price = 60,
                Icon = "/Assets/Hotels/epic.png",
            },
             new MapSchema {
                Id = "10",               
                Title = "Hotel Panorama",
                Address = "Avlabari, Samreklo (Uritski) Street 26, Tbilisi",
                Latitude = 44.814256,
                Longtitude = 41.693181, 
                Price = 55,
                Icon = "/Assets/Hotels/panorama.png",
            }
		};
        private  IEnumerable<MapSchema> _data;
        private IEnumerable<MapSchema> _selectedHotels = _hotelData;
        public async Task<IEnumerable<MapSchema>> LoadData() {
            _data = _selectedHotels;
            return null;
        }

        public async Task<IEnumerable<MapSchema>> Refresh() {
            await HotelsNearMe();
            return await LoadData();
        }
        private async Task HotelsNearMe() 
        {
            List<MapSchema> nearMe = new List<MapSchema>();
            Geocoordinate mypos = await PositionDataProvider.MyPosition();
            foreach (MapSchema hotel in _hotelData)
            {
                if (DistanceToHotel(mypos.Longitude, mypos.Latitude, hotel.Latitude, hotel.Longtitude) < 5)
                {
                    nearMe.Add(hotel);
                }
            }
            _selectedHotels = nearMe;
        }
        private int DistanceToHotel(double myLat, double myLon, double hotelLat, double hotelLon) 
        {
            short R = 6371; // Radius of the earth in km
            double degreeLat = DegreeToRadian(hotelLat - myLat);  // deg2rad below
            double degreeLon = DegreeToRadian(hotelLon - myLon);
            double a =
              Math.Sin(degreeLat / 2) * Math.Sin(degreeLat / 2) +
              Math.Cos(DegreeToRadian(myLat)) * Math.Cos(DegreeToRadian(hotelLat)) *
              Math.Sin(degreeLon / 2) * Math.Sin(degreeLon / 2)
              ;
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            int distance = Convert.ToInt32(R * c); // Distance in km
            return distance;
        }

        private double DegreeToRadian(double degree)
        {
            return degree * (Math.PI / 180);
        }
    }
}
