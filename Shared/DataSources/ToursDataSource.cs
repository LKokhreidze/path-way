using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;
using System.Xml.Serialization;
using System.Linq;
using System.Threading;
using System.Net.Http;
using System.Text;
using Microsoft.Phone.Net.NetworkInformation;

namespace AppStudio.Data
{
    public class ToursDataSource : IDataSource<ToursSchema>
    {
        public static string searchNames = "";
        private StorageFolder _local = ApplicationData.Current.LocalFolder;
        private StorageFolder _localFolder;
        private StorageFile _xmlFile;
        private TourList.TourData _localTours;
        private IEnumerable<ToursSchema> _data;
        private List<ToursSchema> _toursList = new List<ToursSchema>();
        private List<ToursSchema> _searchResult = new List<ToursSchema>();

        private void LoadTours()
        {
            foreach (var item in _localTours.Tour)
            {
                ToursSchema tourObj = new ToursSchema()
                {
                    Id = item.Id.ToString(),
                    Title = item.Name,
                    Subtitle = item.Type,
                    Image = item.TourImg,
                    KMLUrl = item.TourUrl,
                    Description = item.Description,
                };
                _toursList.Add(tourObj);
            }
            _data = _toursList;
        }

        public async Task<IEnumerable<ToursSchema>> LoadData()
        {
            _toursList.Clear();
            await GetFile();
            return null;
        }

        public async Task<IEnumerable<ToursSchema>> Refresh()
        {
            return await Task.Run(() =>
            {
                if (searchNames == "")
                {
                    _data = _toursList;
                }
                else
                {
                    _searchResult.Clear();
                    var keywords = searchNames.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in keywords)
                    {
                        var results = (from u in _toursList
                                       where u.Title.ToLower().Contains(item.ToLower()) || u.Subtitle.ToLower().Contains(item.ToLower())
                                       select u).Distinct().ToList();
                        if (results != null)
                        {
                            results.ForEach(o => _searchResult.Add(new ToursSchema()
                            {
                                Id = o.Id.ToString(),
                                Title = o.Title,
                                Subtitle = o.Subtitle,
                                Image = o.Image,
                                KMLUrl = o.KMLUrl,
                                Description = o.Description,
                            })
                            );
                        }
                    }
                    if (_searchResult.Count > 0)
                    {
                        _data = _searchResult.Distinct();
                    }
                    else
                    {
                        _searchResult.Add(new ToursSchema()
                        {
                            Title = "No results found"
                        });
                        _data = _searchResult;
                    }
                }
                Thread.Sleep(1000);
                return _data;
            });
        }

        public static bool CheckNetworkConnection()
        {
            var ni = NetworkInterface.NetworkInterfaceType;
            bool IsConnected = false;
            if ((ni == NetworkInterfaceType.Wireless80211) || (ni == NetworkInterfaceType.MobileBroadbandCdma) || (ni == NetworkInterfaceType.MobileBroadbandGsm))
                IsConnected = true;
            else if (ni == NetworkInterfaceType.None)
                IsConnected = false;
            return IsConnected;
        }

        private async Task GetFile()
        {
            if (CheckNetworkConnection())
            {
                HttpClient _client = new HttpClient();
                _client.DefaultRequestHeaders.IfModifiedSince = DateTime.Now.AddDays(+1);
                string result = await _client.GetStringAsync(new Uri("http://lamerwebmaster.dsl.ge/developer/TourInfo.xml"));
                byte[] byteArray = Encoding.UTF8.GetBytes(result);
                Stream stream = new MemoryStream(byteArray);
                client_CheckXml(stream);
            }
            else
            {
                _localFolder = await _local.CreateFolderAsync("TourFolder", CreationCollisionOption.OpenIfExists);
                if (File.Exists(Path.Combine(_localFolder.Path, "Tours.xml")))
                {
                    _xmlFile = await _localFolder.CreateFileAsync("Tours.xml", CreationCollisionOption.OpenIfExists);
                    using (Stream str = await _xmlFile.OpenStreamForReadAsync())
                    {
                        TourList.TourData xmldoc = XmlDeserialization(str);
                        foreach (var item in xmldoc.Tour)
                        {
                            ToursSchema tourObj = new ToursSchema()
                            {
                                Id = item.Id.ToString(),
                                Title = item.Name,
                                Subtitle = item.Type,
                                Image = "/Assets/NoImage.png",
                                KMLUrl = item.TourUrl,
                                Description = item.Description,
                            };
                            _toursList.Add(tourObj);
                        }
                        _data = _toursList;
                    }
                }
                else
                {
                    ToursSchema tourObj = new ToursSchema();
                    tourObj.Title = "you need Internet connection for first use";
                    _toursList.Add(tourObj);
                    _data = _toursList;
                }
            }
        }

        private async void client_CheckXml(Stream stream)
        {
            _localFolder = await _local.CreateFolderAsync("TourFolder", CreationCollisionOption.OpenIfExists);
            _xmlFile = await _localFolder.CreateFileAsync("Tours.xml", CreationCollisionOption.OpenIfExists);
            TourList.TourData xmldoc = XmlDeserialization(stream);
            if (new FileInfo(_xmlFile.Path).Length != 0)
            {
                await FindDifferences(xmldoc);
                LoadTours();
            }
            else
            {
                WriteInLocalFile(xmldoc);
                LoadTours();
            }
        }

        private TourList.TourData XmlDeserialization(Stream content)
        {
            using (TextReader reader = new StreamReader(content))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(TourList.TourData));
                object obj = deserializer.Deserialize(reader);
                TourList.TourData KmlData = (TourList.TourData)obj;
                return KmlData;
            }
        }

        private void WriteInLocalFile(TourList.TourData source)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TourList.TourData));
            using (FileStream s = File.Create(_xmlFile.Path))
            {
                serializer.Serialize(s, source);
            }
            _localTours = source;
        }

        private async Task FindDifferences(TourList.TourData source)
        {
            if (_local != null)
            {
                using (Stream fileToRead = await _localFolder.OpenStreamForReadAsync("Tours.xml"))
                {
                    _localTours = XmlDeserialization(fileToRead);
                    IEnumerable<TourList.Tour> dif = source.Tour.Except(_localTours.Tour, new TourList.TourComparer()).ToList();
                    if (dif != null)
                    {
                        _localTours.Tour.AddRange(dif);
                        WriteInLocalFile(_localTours);
                    }
                }
            }
        }

        public async Task Search(string txt)
        {
            searchNames = txt;
            await Refresh();
        }  
    }
}
