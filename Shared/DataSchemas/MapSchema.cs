using System;

namespace AppStudio.Data
{
    /// <summary>
    /// Implementation of the MapSchema class.
    /// </summary>
    public class MapSchema : BindableSchemaBase
    {

        private string _address;
        private string _title;
        private string _icon;
        private double _lon;
        private double _lat;
        private double _price;

        public string Address
        {
            get { return _address; }
            set { SetProperty(ref _address, value); }
        }

        public string FullInfo
        {
            get 
            { 
                return String.Format("Address: {0}\nAverage Price: {1} USD",Address,Price); 
            }
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        public string Icon
        {
            get { return _icon; }
            set { SetProperty(ref _icon, value); }
        }

        public double Latitude
        {
            get { return _lat; }
            set { SetProperty(ref _lat, value); }
        }
        public double Longtitude
        {
            get { return _lon; }
            set { SetProperty(ref _lon, value); }
        }

        public double Price
        {
            get { return _price; }
            set { SetProperty(ref _price, value); }
        }
        public override string DefaultTitle
        {
            get { return Title; }
        }

        public override string DefaultSummary
        {
            get { return null; }
        }

        public override string DefaultImageUrl
        {
            get { return Icon; }
        }

        override public string GetValue(string fieldName)
        {
            if (!String.IsNullOrEmpty(fieldName))
            {
                switch (fieldName.ToLowerInvariant())
                {
                    case "name":
                        return String.Format("{0}", Address);
                    case "title":
                        return String.Format("{0}", Title);
                    case "icon":
                        return String.Format("{0}", Icon);
                    case "defaulttitle":
                        return String.Format("{0}", DefaultTitle);
                    case "defaultsummary":
                        return String.Format("{0}", DefaultSummary);
                    case "defaultimageurl":
                        return String.Format("{0}", DefaultImageUrl);
                    default:
                        break;
                }
            }
            return String.Empty;
        }
    }
}
