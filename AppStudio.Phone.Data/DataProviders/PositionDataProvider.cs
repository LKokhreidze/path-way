﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;

namespace AppStudio.Data
{
    static public class PositionDataProvider
    {
        static public async Task<Geocoordinate> MyPosition()
        {
            try
            {
                Geolocator myGeolocator = new Geolocator();
                Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
                return myGeoposition.Coordinate;
            }
            catch (UnauthorizedAccessException e)
            {
                AppLogs.WriteLog("Getting Possition", e.Message, "Exception");
            }
            return null;
        }
    }
}
