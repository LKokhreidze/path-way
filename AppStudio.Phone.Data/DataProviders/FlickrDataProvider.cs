﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Windows.Devices.Geolocation;

namespace AppStudio.Data
{
    public class FlickrDataProvider
    {
        public Uri Image320 { get; set; }

        static private async Task<string> RetrivePhoto()
        {
            Geocoordinate myGeoPosition = await PositionDataProvider.MyPosition();

            HttpClient client = new HttpClient();

            string flickrApiKey = "cecd7b99f3f1ed0f3c8f26fa4471de9a";
            string[] licenses = { "4", "5", "6" };
            string license = String.Join(",", licenses);
            license.Replace(",", "%2C");
            //https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=66e7b28f79b51b23d3431ff9b01e0544&lat=47.7039&lon=-122.333&format=json&nojsoncallback=1
            string url = "https://api.flickr.com/services/rest" +
                         "?method=flickr.photos.search" +
                         "&license = {0}" +
                         "&api_key={1}" +
                         "&lat= {2}" +
                         "&lon= {3}" +
                         "&radius=5" +
                         "&per_page=50" +
                         "&page=1" +
                         "&format=json" +
                         "&nojsoncallback=1";
            string lat = myGeoPosition.Latitude.ToString().Replace(',','.');
            string lon = myGeoPosition.Longitude.ToString().Replace(',', '.'); ;
            string baseUrl = String.Format(url, license, flickrApiKey, lat, lon);

            string returnedValue = await client.GetStringAsync(baseUrl);

            return returnedValue;
        }

        static async public Task<List<FlickrDataProvider>> DisplayPhotos() 
        {
            List<FlickrDataProvider> photoList = new List<FlickrDataProvider>();
            FlickrData apiData = JsonConvert.DeserializeObject<FlickrData>(await RetrivePhoto());
            if (apiData.stat == "ok" )
            {
                foreach (Photo pic in apiData.photos.photo)
                {
                    FlickrDataProvider image = new FlickrDataProvider();
                    //http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
                    //http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
                    string photoUrl = "http://farm{0}.staticflickr.com/{1}/{2}_{3}";
                    string basePhotoUrl = string.Format(photoUrl, pic.farm, pic.server, pic.id, pic.secret);
                    image.Image320 = new Uri(basePhotoUrl + "_n.jpg", UriKind.Absolute);
                    photoList.Add(image);
                } 
            }
            return photoList;
        }
    }
}
