﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace AppStudio.Data {
    public class TourList {
        [XmlRoot]
        public class TourData {
            [XmlElement("Tour")]
            public List<Tour> Tour { get; set; }
        }

        public class Tour {
            [XmlAttribute("ID")]
            public int Id { get; set; }
            [XmlElement("Name")]
            public string Name { get; set; }
            [XmlElement("Type")]
            public string Type { get; set; }
            [XmlElement("Description")]
            public string Description { get; set; }
            [XmlElement("TourUrl")]
            public string TourUrl { get; set; }
            [XmlElement("TourImg")]
            public string TourImg { get; set; }

        }

        public class TourComparer : IEqualityComparer<Tour> {
            public bool Equals(Tour x, Tour y) {
                if (object.ReferenceEquals(x, y)) {
                    return true;
                }
                if (object.ReferenceEquals(x, null) ||
                    object.ReferenceEquals(y, null)) {
                    return false;
                }
                return x.Id == y.Id;
            }

            public int GetHashCode(Tour tour) {
                if (tour == null) {
                    return 0;
                }

                return tour.Id.GetHashCode();
            }
        }
    }
}
