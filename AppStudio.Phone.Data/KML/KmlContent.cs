﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Device.Location;
using System.Windows;
using Windows.Storage;
using System.Threading.Tasks;
using AppStudio;

namespace Kml {
    public class KmlContent {
        public kml Data { get; set; }
        private StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
        private StorageFolder localFolder;
        private StorageFile file;

        public KmlContent() {
            // Data = DeserializeKml(info);
        }

        public kml DeserializeKml(Stream path) {
            XmlSerializer deserializer = new XmlSerializer(typeof(kml));
            try {
                using (TextReader reader = new StreamReader(path)) {
                    object obj = deserializer.Deserialize(reader);
                    kml KmlData = (kml)obj;
                    return KmlData;
                }
            } catch (Exception e) {
                AppLogs.WriteError("DeserializeKML Method:", e.Message);
                return null;
            }
        }

        public async Task<kml> ReadFiles(string kmlName) {
            if (local != null) {
                try {
                    localFolder = await local.GetFolderAsync("KMLFolder");
                    file = await localFolder.GetFileAsync(kmlName);

                    using (Stream streamReader = await file.OpenStreamForReadAsync()) {
                        Data = DeserializeKml(streamReader);
                    }
                } catch (FileNotFoundException) {
                    throw new FileNotFoundException();
                }
            }
            return Data;
        }

        public List<GeoCoordinate> ParseLocation(string location) {
            List<GeoCoordinate> locationList = null;
            locationList = new List<GeoCoordinate>();
            char[] delimiters = new char[] { '\n', };
            string[] coordiantes = location.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (coordiantes.Length > 0) {
                foreach (string item in coordiantes) {
                    if (!string.IsNullOrWhiteSpace(item)) {
                        string[] cord = item.Trim().Split(',');
                        //if (Microsoft.Devices.Environment.DeviceType == Microsoft.Devices.DeviceType.Emulator) {
                            locationList.Add(new GeoCoordinate(Double.Parse(cord[1]), Double.Parse(cord[0])));
                        //} else if (Microsoft.Devices.Environment.DeviceType == Microsoft.Devices.DeviceType.Device) {
                           // locationList.Add(new GeoCoordinate(Double.Parse(cord[1].Replace(".", ",")), Double.Parse(cord[0].Replace(".", ","))));
                        //}
                    }
                }
            }
            return locationList;
        }
    }
}
