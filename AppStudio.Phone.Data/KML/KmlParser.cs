﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Kml
{
    [XmlRoot(Namespace = @"http://earth.google.com/kml/2.2")]
    public class kml:TourIdKML
    {
        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces xmlns;
        [XmlElement("Document")]
        public Document Document { get; set; }

    }
    public class Document
    {
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlElement("Style")]
        public List<Style> Styles { get; set; }
        [XmlElement("Placemark")]
        public List<Placemark> Placemarks { get; set; }
    }

    public class Style
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("LineStyle")]
        public LineStyle LineStyle { get; set; }
        [XmlElement("Iconstyle")]
        public IconStyle Iconstyle { get; set; }

    }
    public class LineStyle
    {
        [XmlElement("color")]
        public string Color { get; set; }
        [XmlElement("width")]
        public int Width { get; set; }

    }
    public class IconStyle
    {
        [XmlElement("Icon")]
        public Icon Icon { get; set; }
    }
    public class Icon
    {
        [XmlElement("href")]
        public string Href { get; set; }
    }
    public class Placemark
    {
        
        private string styleUrlField;
        private char[] charsToTrim = { '#' };
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlElement("LineString")]
        public LineString LineString { get; set; }
        [XmlElement("styleUrl")]
        public string StyleUrl 
        {
            get
            {
                return this.styleUrlField.Replace("#style", "");
            }
            set
            {
                this.styleUrlField = value;
            } 
        }
        [XmlElement("Point")]
        public Point Point { get; set; }

    }
    public class LineString
    {
        [XmlElement("tessellate")]
        public int Tessellate { get; set; }
        [XmlElement("coordinates")]
        public string Coordinates { get; set; }
    }
    public class Point
    {
        [XmlElement("coordinates")]
        public string Coordinates { get; set; }
    }
}
