﻿using System;

namespace MobileBearing
{
    public class AngleConverter : IAngleConverter
    {
        public double ConvertDegreesToRadians(double degrees)
        {
            return Math.PI * degrees / 180.0;
        }
        public double ConvertRadiansToDegrees(double radians)
        {
            return 180.0 * radians / Math.PI;
        }
    }
}
