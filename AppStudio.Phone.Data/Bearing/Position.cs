﻿using System.Device.Location;
namespace MobileBearing
{
    public class Position
    {

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Position(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public Position(GeoCoordinate coord)
        {
            Latitude = coord.Latitude;
            Longitude = coord.Longitude;
        }

    }
}
